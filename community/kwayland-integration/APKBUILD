# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland-integration
pkgver=5.21.2
pkgrel=0
pkgdesc="KWayland integration"
url="https://kde.org/plasma-desktop/"
arch="all !armhf" # armhf blocked by extra-cmake-modules
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kglobalaccel"
makedepends="
	extra-cmake-modules
	kguiaddons-dev
	kidletime-dev
	kwayland-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-integration-$pkgver.tar.xz"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="e29a82b560f0bc489d78c5493d2e35b52f810a9243aa18f23a94552045507b6835971ac4a1a57ae3683b00460dcae3b98b32c437bbb0e2f3ee440512bdc4ef01  kwayland-integration-5.21.2.tar.xz"
