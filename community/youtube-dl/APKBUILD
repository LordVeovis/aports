# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=youtube-dl
pkgver=2021.03.02
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube"
url="https://youtube-dl.org/"
arch="noarch"
license="Unlicense"
depends="python3"
checkdepends="py3-flake8 py3-nose"
subpackages="$pkgname-doc
	$pkgname-zsh-completion:zshcomp
	$pkgname-bash-completion
	$pkgname-fish-completion"
source="https://youtube-dl.org/downloads/$pkgver/youtube-dl-$pkgver.tar.gz"
builddir="$srcdir/$pkgname"

prepare() {
	default_prepare
	sed -i \
		-e 's|etc/bash_completion.d|share/bash-completion/completions|' \
		-e 's|etc/fish/completions|share/fish/completions|' \
		"$builddir"/setup.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

zshcomp() {
	pkgdesc="Zsh completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	install -Dm644 "$builddir"/$pkgname.zsh \
		"$subpkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="1318434c009a9eb41231bb7897b16fd72fde5d3165f6a24ff97031ed89b4d6408520242c71270f12b4f55dfa97ca48d42c6f167ad036eca74241e61cb950c298  youtube-dl-2021.03.02.tar.gz"
