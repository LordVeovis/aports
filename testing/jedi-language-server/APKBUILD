# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=jedi-language-server
pkgver=0.25.6
pkgrel=0
pkgdesc="Python language server exclusively for Jedi"
url="https://github.com/pappasam/jedi-language-server"
arch="noarch"
license="MIT"
depends="python3 py3-click py3-jedi py3-gls py3-docstring-to-markdown"
makedepends="py3-pip poetry"
checkdepends="py3-pytest py3-pyhamcrest py3-jsonrpc-server"
source="$pkgname-$pkgver.tar.gz::https://github.com/pappasam/jedi-language-server/archive/v$pkgver.tar.gz"

build() {
	# poetry is buggy and doesn't include python source files if it is built in
	# the default directory...?
	mkdir -p /tmp/$pkgname && cp -r . /tmp/$pkgname && cd /tmp/$pkgname
	poetry build --format wheel
}

check() {
	PYTHONPATH="." pytest
}

package() {
	python3 -m pip install \
		--no-warn-script-location --ignore-installed --no-deps --root="$pkgdir" \
		/tmp/$pkgname/dist/jedi_language_server-$pkgver-py3-none-any.whl
}

sha512sums="145c2c35ffe2531eef5cd8d970587fa7c386affe0e88bd9cae30a1d6f1c9c15a1fbfef4e7bfb694bc60413e20745b8a77eb5772a021ce4dbe08fddc67fcde995  jedi-language-server-0.25.6.tar.gz"
