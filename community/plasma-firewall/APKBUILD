# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-firewall
pkgver=5.21.2
pkgrel=0
pkgdesc="Control Panel for your system firewall"
# armhf blocked by qt5-qtdeclarative
# s390x and mips64 blocked by kcmutils
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	python3
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-firewall-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="6163c7367cc376f7f291e8b175b3d4df8e209199cc393c9d4d9246926a5f275dcb6181411d3d4a969293c6a7ec3c4bc8723fdfc4b79bfd15bf022421fc76e170  plasma-firewall-5.21.2.tar.xz"
