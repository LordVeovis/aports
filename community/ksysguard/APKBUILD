# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksysguard
pkgver=5.21.2
pkgrel=0
pkgdesc="Track and control the processes running in your system"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x !mips64"
url="https://userbase.kde.org/KSysGuard"
license="GPL-2.0-only"
makedepends="
	eudev-dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	kwindowsystem-dev
	libksysguard-dev
	libnl3-dev
	libpcap-dev
	lm-sensors-dev
	networkmanager-qt-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/ksysguard-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ksystemstatstest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ksystemstatstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="785ff5b6faaca4169af0bf389c1ebcefa68d21839945004670691481c19bee88cb3b821627ddc5fbbc353443cf63c03fba01f09fee41547e04fc29262e75c448  ksysguard-5.21.2.tar.xz"
